#pragma checksum "C:\Users\Nityanando Malo\source\repos\BoatRenting\BoatRenting\Views\UserRegistration\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8dab8b432b6ce9857fd5d3e8ae381e7b33d01437"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_UserRegistration_Index), @"mvc.1.0.view", @"/Views/UserRegistration/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Nityanando Malo\source\repos\BoatRenting\BoatRenting\Views\_ViewImports.cshtml"
using BoatRenting;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Nityanando Malo\source\repos\BoatRenting\BoatRenting\Views\_ViewImports.cshtml"
using BoatRenting.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8dab8b432b6ce9857fd5d3e8ae381e7b33d01437", @"/Views/UserRegistration/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"be59a9ebb509af7c14acd50f3e70d04616d21a9e", @"/Views/_ViewImports.cshtml")]
    public class Views_UserRegistration_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Create", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "C:\Users\Nityanando Malo\source\repos\BoatRenting\BoatRenting\Views\UserRegistration\Index.cshtml"
  
    ViewData["Title"] = "User Registration";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<h1 class=""text-center"">User Registration</h1>
<div class=""container"">
    <div class=""row"">
        <div class=""col-md-12"">
            <div class=""form-group"">
                <label class=""MessageAlert"" style=""color:green;font-weight:bolder"" class=""Message-Alert control-label"">");
#nullable restore
#line 11 "C:\Users\Nityanando Malo\source\repos\BoatRenting\BoatRenting\Views\UserRegistration\Index.cshtml"
                                                                                                                  Write(TempData["SuccessMessage"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</label>\r\n                <label class=\"MessageAlert\" style=\"color:red;font-weight:bolder\" class=\"Message-Alert control-label\">");
#nullable restore
#line 12 "C:\Users\Nityanando Malo\source\repos\BoatRenting\BoatRenting\Views\UserRegistration\Index.cshtml"
                                                                                                                Write(TempData["ErrorMessage"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</label>\r\n            </div>\r\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "8dab8b432b6ce9857fd5d3e8ae381e7b33d014375059", async() => {
                WriteLiteral(@"

                <div class=""form-group"">
                    <label class=""control-label"">User Name</label>
                    <input type=""text"" name=""Username"" required class=""form-control"" />
                </div>
                <div class=""form-group"">
                    <label class=""control-label"">First Name</label>
                    <input type=""text"" name=""FirstName"" required class=""form-control"" />
                </div>
                <div class=""form-group"">
                    <label class=""control-label"">Last Name</label>
                    <input type=""text"" name=""LastName"" required class=""form-control"" />
                </div>
                <div class=""form-group"">
                    <label class=""control-label""> Password</label>
                    <input name=""Password"" type=""password"" required class=""form-control"" />
                </div>
                <div class=""form-group"">
                    <label class=""control-label"">Contact No</label>
           ");
                WriteLiteral(@"         <input type=""text"" name=""ContactNo"" class=""form-control"" />
                </div>
                <div class=""form-group"">
                    <label class=""control-label"">Address</label>
                    <input type=""text"" name=""Address"" class=""form-control"" />
                </div>
                <div class=""form-group"">
                    <input type=""submit"" id=""createUser"" value=""Create"" class=""btn btn-primary"" />
                </div>

            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n        </div>\r\n    </div>\r\n</div>\r\n<script>\r\n    setTimeout(function () {\r\n        $(\".MessageAlert\").fadeOut(5000);\r\n    },5000);\r\n</script>\r\n\r\n\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
