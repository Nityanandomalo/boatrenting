﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BoatRenting.Models
{
    public partial class BoatRentContext : DbContext
    {
        public BoatRentContext()
        {
        }

        public BoatRentContext(DbContextOptions<BoatRentContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BoatMaster> BoatMaster { get; set; }
        public virtual DbSet<RentMaster> RentMaster { get; set; }
        public virtual DbSet<UserMaster> UserMaster { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BoatMaster>(entity =>
            {
                entity.HasKey(e => e.BoatId);

                entity.ToTable("boat_master");

                entity.Property(e => e.BoatId).HasColumnName("boat_id");

                entity.Property(e => e.BoatColor)
                    .HasColumnName("boat_color")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BoatImage)
                    .HasColumnName("boat_image")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BoatName)
                    .IsRequired()
                    .HasColumnName("boat_name")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.CreationDate).HasColumnName("creation_date");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasColumnName("is_active")
                    .HasMaxLength(1)
                    .IsUnicode(false);
                entity.Property(e => e.IsRented)
                   .HasColumnName("is_rented")
                   .HasMaxLength(1)
                   .IsUnicode(false);
                entity.Property(e => e.RentRate)
                    .HasColumnName("rent_rate")
                    .HasColumnType("money");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.BoatMaster)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_boat_master_user_master");
            });

            modelBuilder.Entity<RentMaster>(entity =>
            {
                entity.HasKey(e => e.RentId);

                entity.ToTable("rent_master");

                entity.Property(e => e.RentId).HasColumnName("rent_id");

                entity.Property(e => e.BoatId).HasColumnName("boat_id");

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creation_date")
                    .HasColumnType("datetime");
                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasColumnName("is_active")
                    .HasMaxLength(1)
                    .IsUnicode(false);
                entity.Property(e => e.RentFor)
                    .HasColumnName("rent_for")
                    .HasColumnType("decimal(3, 2)");
                entity.Property(e => e.TotalAmount)
                    .HasColumnName("TotalAmount")
                    .HasColumnType("decimal(18, 2)");
                entity.Property(e => e.RentOn)
                    .HasColumnName("rent_on")
                    .HasColumnType("datetime");

                entity.Property(e => e.RentTo)
                    .IsRequired()
                    .HasColumnName("rent_to")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Boat)
                    .WithMany(p => p.RentMaster)
                    .HasForeignKey(d => d.BoatId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_rent_master_boat_master");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.RentMaster)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_rent_master_user_master");
            });

            modelBuilder.Entity<UserMaster>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("user_master");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.Address)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNo)
                    .HasColumnName("contact_no")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creation_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FirstName)
                    .HasColumnName("first_name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive)
                    .HasColumnName("is_active")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasColumnName("last_name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.UpdationDate)
                    .HasColumnName("updation_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
