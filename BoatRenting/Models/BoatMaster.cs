﻿using System;
using System.Collections.Generic;

namespace BoatRenting.Models
{
    public partial class BoatMaster
    {
        public BoatMaster()
        {
            RentMaster = new HashSet<RentMaster>();
        }

        public long BoatId { get; set; }
        public string BoatName { get; set; }
        public decimal RentRate { get; set; }
        public string BoatColor { get; set; }
        public string BoatImage { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public string IsActive { get; set; }
        public string IsRented { get; set; }
        public virtual UserMaster CreatedByNavigation { get; set; }
        public virtual ICollection<RentMaster> RentMaster { get; set; }
    }
}
