﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BoatRenting.Models
{
    public class BoatRegistrationModel
    {
        public string BoatName { get; set; }
        public decimal BoatRent { get; set; }
        public string BoatColor { get; set; }
        public string BoatImage { get; set; }
       
    }
}
