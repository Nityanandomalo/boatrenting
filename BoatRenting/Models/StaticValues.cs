﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BoatRenting.Models
{
    public  class StaticValues: IStaticValues
    {
        public const string Active = "A";
        public const string DeActive = "D";
        public const string BoatCurentlyRented = "Y";
        public const string BoatCurentlyAvailable = "N";
        public string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
        public decimal CalculateAmount(DateTime RentedOn, decimal HourlyRate)
        {
            decimal ActualAmount = 0;

            TimeSpan TotalTime = DateTime.Now- RentedOn ;
            decimal MinutesRate = HourlyRate / 60;

            if (TotalTime.Hours > 0)
            {
                ActualAmount = ActualAmount + (TotalTime.Hours * HourlyRate);
            }
            else if (TotalTime.Minutes > 0)
            {
                ActualAmount = ActualAmount + (TotalTime.Minutes * MinutesRate);
            }

            return ActualAmount;

        }
    }
}
