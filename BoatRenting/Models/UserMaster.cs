﻿using System;
using System.Collections.Generic;

namespace BoatRenting.Models
{
    public partial class UserMaster
    {
        public UserMaster()
        {
            BoatMaster = new HashSet<BoatMaster>();
            RentMaster = new HashSet<RentMaster>();
        }

        public long UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? UpdationDate { get; set; }
        public string Password { get; set; }
        public string ContactNo { get; set; }
        public string Address { get; set; }

        public virtual ICollection<BoatMaster> BoatMaster { get; set; }
        public virtual ICollection<RentMaster> RentMaster { get; set; }
    }
}
