﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BoatRenting.Models
{
    public class RentBoat
    {
        public long BoatId { get; set; }
        public string BoatName { get; set; }
        public string BoatColor { get; set; }
        public string RentRate { get; set; }
        public string Status { get; set; }
        public string IsRented { get; set; }
        public string RentedOn { get; set; }
        public string AvailableTime { get; set; }
        public string RentTo { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? RentedFor { get; set; }
    }
}
