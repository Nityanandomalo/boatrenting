﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BoatRenting.Models
{
    public class UserRegitrationModel
    {
        
        [Required(ErrorMessage = "This field is required.")]
        [DisplayName("User Name")]
        public string Username { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [DisplayName("First Name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [DisplayName("Last Name")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [DisplayName("Password")]
        public string Password { get; set; }
        [DisplayName("Contact No.")]
        public string ContactNo { get; set; }
        [DisplayName("Address")]
        public string Address { get; set; }
    }
}
