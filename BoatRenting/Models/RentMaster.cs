﻿using System;
using System.Collections.Generic;

namespace BoatRenting.Models
{
    public partial class RentMaster
    {
        public long RentId { get; set; }
        public string RentTo { get; set; }
        public DateTime RentOn { get; set; }
        public long BoatId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public decimal? RentFor { get; set; }
        public decimal? TotalAmount { get; set; }
        
        public string IsActive { get; set; }
        public virtual BoatMaster Boat { get; set; }
        public virtual UserMaster CreatedByNavigation { get; set; }
    }
}
