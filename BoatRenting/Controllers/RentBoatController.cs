﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoatRenting.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BoatRenting.Controllers
{
    public class RentBoatController : Controller
    {
        private readonly BoatRentContext _context;
        private string _UserId;
        const string UserId = "UserId";
        private readonly IStaticValues _staticValues;
        public RentBoatController(BoatRentContext context,IStaticValues staticValues)
        {
            _context = context;
            _staticValues = staticValues;
        }
        public async Task<IActionResult> Index()
        {
            try
            {
                _UserId = HttpContext.Session.GetString(UserId);
                if (!String.IsNullOrEmpty(_UserId))
                {
                    var objBoatLst = _context.BoatMaster.Select(x => new RentBoat
                    {
                        BoatId = x.BoatId,
                        BoatName = x.BoatName,
                        BoatColor = x.BoatColor,
                        RentRate = x.RentRate.ToString("0.00"),
                        IsRented = x.IsRented,
                        AvailableTime = "",
                        Status = (x.IsActive == StaticValues.Active) ? "Active" : "Deactive"
                    }).ToListAsync();
                    objBoatLst.Result.ForEach(x =>
                    {
                        if (x.IsRented == StaticValues.BoatCurentlyRented)
                        {
                            var objRentData = _context.RentMaster.Where(z => z.BoatId == x.BoatId && z.IsActive == StaticValues.Active).FirstOrDefault();
                            if (objRentData.RentFor.Value > 0)
                            {
                                var rentedOn = objRentData.RentOn;
                                DateTime TimeUptoRentedFor = rentedOn.AddHours((double)objRentData.RentFor.Value);
                                TimeSpan TimeRemaining = TimeUptoRentedFor - DateTime.Now;
                                x.AvailableTime = "Available after ";
                                if (TimeRemaining.Hours > 0)
                                {
                                    x.AvailableTime = x.AvailableTime + TimeRemaining.Hours + " Hours ";
                                }
                                if (TimeRemaining.Minutes > 0)
                                {
                                    x.AvailableTime = x.AvailableTime + TimeRemaining.Minutes + " Minutes";
                                }
                                else
                                {
                                    x.AvailableTime = "Soon Available!";
                                }
                            }
                            else
                            {
                                x.AvailableTime = "Availablity time not available!";
                            }
                        }
                        else
                        {
                            if (x.Status == "Active")
                            {
                                x.AvailableTime = "Currently Available!";
                            }
                            else
                            {
                                x.AvailableTime = "Un-Available!";
                            }

                        }
                    });

                    return View(await objBoatLst);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Index", "Home");
            }
            

        }

        public async Task<IActionResult> Active(long? id)
        {
            try
            {
                if (id == null)
                {
                    throw new Exception("No Record Found!");
                }

                var boatMaster = await _context.BoatMaster.Where(x => x.BoatId == (long)id).FirstOrDefaultAsync(); ;
                if (boatMaster == null)
                {
                    throw new Exception("No Record Found!");
                }
                else
                {
                    boatMaster.IsActive = StaticValues.Active;
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction(nameof(Index));
            }
           

        }
        public async Task<IActionResult> DeActive(long? id)
        {
            try
            {
                if (id == null)
                {
                    throw new Exception("No Record Found!");
                }

                var boatMaster = await _context.BoatMaster.Where(x => x.BoatId == (long)id).FirstOrDefaultAsync();
                if (boatMaster == null)
                {
                    throw new Exception("No Record Found!");
                }
                else
                {
                    boatMaster.IsActive = StaticValues.DeActive;
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction(nameof(Index));
            }
            

        }

        public async Task<IActionResult> Rent(long? id)
        {
            var BoatDetails = await _context.BoatMaster.Where(x => x.BoatId == id).Select(x => new RentBoat
            {
                BoatId = x.BoatId,
                BoatName = x.BoatName,
                RentRate = x.RentRate.ToString("0.00"),
                BoatColor = x.BoatColor
            }).FirstOrDefaultAsync();

            return View(BoatDetails);
        }
        [HttpPost]
        public async Task<IActionResult> RentTheBoat(RentBoat objRequest)
        {
            try
            {
                DateTime CurDateTime = DateTime.Now;
                RentMaster objRentMaster = new RentMaster();
                objRentMaster.BoatId = objRequest.BoatId;
                objRentMaster.RentTo = objRequest.RentTo;
                objRentMaster.RentOn = CurDateTime;
                objRentMaster.CreationDate = CurDateTime;
                objRentMaster.RentFor = objRequest.RentedFor;
                objRentMaster.IsActive = StaticValues.Active;
                objRentMaster.CreatedBy = Convert.ToInt64(HttpContext.Session.GetString(UserId));

                var objBoatMaster = _context.BoatMaster.Where(x => x.BoatId == objRequest.BoatId).FirstOrDefault();

                objBoatMaster.IsRented = StaticValues.BoatCurentlyRented;
                _context.Add(objRentMaster);
                await _context.SaveChangesAsync();
                TempData["SuccessMessage"] = "Boat Rented Successfully";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction(nameof(Index));
            }

        }
        [HttpPost]
        public async Task<IActionResult> Details(long? id)
        {
            var BoatDetails = await _context.RentMaster.Where(x => x.BoatId == id && x.IsActive == StaticValues.Active).Select(x => new RentBoat
            {
                BoatId = x.BoatId,
                BoatName = x.Boat.BoatName,
                RentRate = x.Boat.RentRate.ToString("0.00"),
                BoatColor = x.Boat.BoatColor,
                RentTo = x.RentTo,
                RentedOn = x.RentOn.ToString("dd-MMM-yyyy hh:mm tt"),
                RentedFor=x.RentFor
            }).FirstOrDefaultAsync();
            return View(BoatDetails);
        }

        public async Task<IActionResult> CreateBill(long? id)
        {
            try
            {
                var objRentDetals = await _context.RentMaster.Where(x => x.BoatId == id && x.IsActive == StaticValues.Active).Select(x => new RentBoat
                {
                    BoatId = x.BoatId,
                    BoatName = x.Boat.BoatName,
                    RentRate = x.Boat.RentRate.ToString("0.00"),
                    BoatColor = x.Boat.BoatColor,
                    RentTo = x.RentTo,
                    RentedOn = x.RentOn.ToString("dd-MMM-yyyy hh:mm tt"),
                    RentedFor = x.RentFor,
                    TotalAmount = _staticValues.CalculateAmount(x.RentOn, x.Boat.RentRate)
                }).FirstOrDefaultAsync();
                return View(objRentDetals);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction(nameof(Index));
            }
            
        }

        public async Task<IActionResult> CreateBilling(RentBoat objRequest)
        {
            try
            {
                var objBoatRent = _context.RentMaster.Where(x => x.BoatId == objRequest.BoatId && x.IsActive == StaticValues.Active).FirstOrDefaultAsync();

                //Update Status
                objBoatRent.Result.IsActive = StaticValues.DeActive;

                var objBoatMaster = _context.BoatMaster.Where(x => x.BoatId == objRequest.BoatId).FirstOrDefault();
                objBoatMaster.IsRented = StaticValues.BoatCurentlyAvailable;

                await _context.SaveChangesAsync();
                TempData["SuccessMessage"] = "Boat Collected Successfully";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction(nameof(Index));
            }
        }



    }
}
