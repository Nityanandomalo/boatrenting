﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoatRenting.Models;
using Microsoft.AspNetCore.Mvc;

namespace BoatRenting.Controllers
{
    public class UserRegistrationController : Controller
    {
        private readonly BoatRentContext _context;
        private readonly IStaticValues _staticValues;
        public UserRegistrationController(BoatRentContext context,IStaticValues staticValues)
        {
            _context = context;
            _staticValues = staticValues;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(UserRegitrationModel userData)
        {
            try
            {
                bool IsUserAvailable = _context.UserMaster.Any(e => e.Username == userData.Username);
                if (!IsUserAvailable)
                {
                    DateTime CurDateTime = DateTime.Now;
                    UserMaster objUserMaster = new UserMaster();
                    objUserMaster.Username = userData.Username;
                    objUserMaster.FirstName = userData.FirstName;
                    objUserMaster.LastName = userData.LastName;
                    objUserMaster.IsActive = StaticValues.Active;
                    objUserMaster.CreationDate = CurDateTime;
                    objUserMaster.Password = _staticValues.MD5Hash(userData.Password);
                    objUserMaster.ContactNo = userData.ContactNo;
                    objUserMaster.Address = userData.Address;
                    _context.UserMaster.Add(objUserMaster);
                    await _context.SaveChangesAsync();

                    TempData["SuccessMessage"] = "Record Saved Successfully";

                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    throw new Exception("User Alredy Available!");
                }


            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction(nameof(Index));
            }


        }


    }

}
