﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BoatRenting.Models;
using System.Diagnostics.Eventing.Reader;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;

namespace BoatRenting.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IStaticValues _staticValues;
        private readonly BoatRentContext _context;
        private string _UserId;
        const string UserId = "UserId";
        public HomeController(ILogger<HomeController> logger, BoatRentContext context, IStaticValues staticValues)
        {
            _logger = logger;
            _context = context;
            _staticValues = staticValues;
        }

        public IActionResult Index()
        {
            _UserId = HttpContext.Session.GetString(UserId);
            if (!String.IsNullOrEmpty(_UserId))
            {
                return RedirectToAction("Index", "RentBoat");
            }
            else
            {
                return View();
            }
            
        }
        [HttpPost]
        public IActionResult Login(LoginModel LoginData)
        {
            try
            {
                string strPassword = _staticValues.MD5Hash(LoginData.Password);
                var UserData = _context.UserMaster.Where(x => x.Username == LoginData.Username && x.Password == strPassword).FirstOrDefault();
                if (UserData != null)
                {
                    HttpContext.Session.SetString(UserId, UserData.UserId.ToString());
                    return RedirectToAction("Index", "RentBoat");
                }
                else
                {
                    throw new Exception("User Name or Password Not Match!");
                }

            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction(nameof(Index));
            }


        }

        public IActionResult Logout()
        {
            HttpContext.Session.SetString(UserId, "");
            TempData["SuccessMessage"] = "Logout Sucessfully";
            return RedirectToAction(nameof(Index));
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
