﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoatRenting.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BoatRenting.Controllers
{
    public class BoatRegistrationController : Controller
    {
        private readonly BoatRentContext _context;
        private readonly IStaticValues _staticValues;
        private string _UserId;
        const string UserId = "UserId";
        public BoatRegistrationController(BoatRentContext context,IStaticValues staticValues)
        {
            _context = context;
            _staticValues = staticValues;
        }
        public IActionResult Index()
        {


            _UserId = HttpContext.Session.GetString(UserId);
            if (!String.IsNullOrEmpty(_UserId))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }


        }
        [HttpPost]
        public async Task<IActionResult> Register(BoatRegistrationModel BoatData)
        {
            try
            {
                DateTime currDate = DateTime.Now;
                DateTime CurDateTime = DateTime.Now;
                BoatMaster objBoatMaster = new BoatMaster();
                objBoatMaster.BoatName = BoatData.BoatName;
                objBoatMaster.RentRate = BoatData.BoatRent;
                objBoatMaster.BoatColor = BoatData.BoatColor;
                objBoatMaster.CreatedBy = Convert.ToInt64(HttpContext.Session.GetString(UserId));
                objBoatMaster.CreationDate = currDate;
                objBoatMaster.IsRented = StaticValues.BoatCurentlyAvailable;
                objBoatMaster.IsActive = StaticValues.Active;
                _context.BoatMaster.Add(objBoatMaster);
                await _context.SaveChangesAsync();
                TempData["BoatID"] = " having Boat Id:"+ objBoatMaster.BoatId.ToString();
                TempData["SuccessMessage"] = "Record Saved Successfully";
                return RedirectToAction("Index", "RentBoat");
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction(nameof(Index));
            }


        }
    }
}
